# Randit - Aluguel de ferramentas e equipamentos #

Projeto iniciado com visão a conclusão de disciplina de desenvolvimento e inovação na Faculdade,
desenvolvido em AngularJs e deploy feito na Amazon S3.

---

## Contributions and tutorials

Tutorial para contribuição no projeto

### Starting

1. Requisitos Nodejs + npm.
2. Dentro da pasta raiz do projeto executar o comando via terminal: *npm install*
3. Executar o comando *npm run dev*
4. Aplicação estará rodando em *localhost:3000*

### New Feature

1. Clonar o repositório / nova branch a partir da **master**;
2. Criar uma branch com nome da feature seguindo o padrão: 'dev-new-login', 'dev-product-screen';
3. Desenvolver a feature;
4. Realizar o pull-request para development e merge;
5. Realizar testes;
6. Pull-request para master com título seguindo o padrão: '[ RELEASE ] Version 1.0';

### Hot Fixes

1. Criar nova branch a partir da **master** seguindo o padrão: 'fix-response-code', 'fix-authentication';
2. Resolver o bug;
3. Realizar o pull-request para development e merge;
4. Realizar testes;
5. Pull-request para master com título seguindo o padrão: '[ HOTFIX ] Fixed wrong status code return';

---

## Documents

A documentação e o processo de desenvolvimento está sendo controlado no [Trello](https://trello.com/b/rqx6oav2/projeto-aluguei).

Pasta com documentos e controles em [Google Drive](https://drive.google.com/drive/folders/0AH88MPeMrxDWUk9PVA)

---

## Structure

```

 ___ node_modules *bibliotecas via npm*
|
|___ index.html *página raiz da aplicação*
|
|___ src *conteúdo da aplicação*
    |
    |__ assets *imagens*
    |
    |__ app *Páginas*
    |
    |__ services *API's e webservices*
    |
    |__ environments *Variáveis de ambiente*
```

---

## Author

- Ailton Vieira (ailtonsp@hotmail.com)

## Contributors

- Luan Santos (luan296@gmail.com)
