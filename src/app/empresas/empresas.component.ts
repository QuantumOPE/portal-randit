import { Component, OnInit } from '@angular/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { CompanyService } from '../../services/company.service';
import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.css']
})
export class EmpresasComponent implements OnInit {

  constructor(private companyApi: CompanyService, public dialog: MatDialog, private formBuilder: FormBuilder) {
  }

  companyList;
  submit = false;

  companySelected = {
    "id": 0,
    "flagAtivo": false,
    "nomeFantasia": '',
    "razaoSocial": '',
    "numeroCnpj": '',
    "inscricaoEstadual": '',
    "inscricaoMunicipal": '',
    "tipoContribuinte": '',
    "atividadeEmpresa": '',
    "descricao": '',
    "idEmpresaMatriz": 0,
    "idContatoEndereco": 0,
    "idContatoTelefone": 0,
    "dtCadastro": '',
  }

  companyModel = {
    "id": 0,
    "flagAtivo": false,
    "nomeFantasia": '',
    "razaoSocial": '',
    "numeroCnpj": '',
    "inscricaoEstadual": '',
    "inscricaoMunicipal": '',
    "tipoContribuinte": '',
    "atividadeEmpresa": '',
    "descricao": '',
    "idEmpresaMatriz": 0,
    "idContatoEndereco": 0,
    "idContatoTelefone": 0,
    "dtCadastro": ''
  }

  async listCompanys() {
    this.companyList = await this.companyApi.listCompany();
    this.companyList = JSON.parse(this.companyList).results;
    this.companySelected = this.companyModel;
  }

  selectCompany(company) {
    this.companySelected = company;
  }

  newCompany() {
    this.companySelected = this.companyModel;
    let now = new Date();
    this.companySelected.dtCadastro = now.getDate() + '/' + ((+now.getMonth() + 1) < 10 ? '0' + (now.getMonth() + 1) : (now.getMonth() + 1)) + '/' + now.getFullYear()
  }

  async saveCompany(company) {
    if (company.id != 0) {
      await this.companyApi.updateCompany(company).then((response) => {
        this.reallySave({ title: "Sucesso!", body: "Atualizações salvas.", ok: "Ok" })
      })
        .catch((error) => {
          this.reallySave({ title: "Erro!", body: "Não foi possível realizar a operação, tente novamente.", ok: "Ok" })

        });
    } else {
      await this.companyApi.insertCompany(company).then((response) => {
        this.reallySave({ title: "Sucesso!", body: "Empresa criada!", ok: "Ok" })
      })
        .catch((error) => {
          this.reallySave({ title: "Erro!", body: "Não foi possível realizar a operação, tente novamente.", ok: "Ok" })

        })
    }
    this.listCompanys()
  }

  async deleteCompany(company) {
    this.companySelected = company;
    this.submit = true;
    this.openDialog({ title: "Deletar Empresa!", body: "Tem certeza que deseja deletar esta empresa ?", ok: "Deletar", cancel: "Cancelar" })
  }

  async ngOnInit() {
    this.listCompanys();

  }


  openDialog(data): void {
    var dialogref = this.dialog.open(DialogComponent, {
      width: '30%',
      data: data,
    });

    dialogref.afterClosed().subscribe(async (data) => {
      if (data == 'ok' && this.submit == true) {
        await this.companyApi.deleteCompany(this.companySelected.id).then((response) => {
          this.openDialog({ title: "Sucesso!", body: "Empresa deletada.", ok: "Ok" })
          this.submit = false;
        })
          .catch((error) => {
            this.openDialog({ title: "Error!", body: "Não foi possível realizar a operação, tente novamente.", ok: "Ok" })
          })
          this.listCompanys()
      }
    });
  }

  reallySave(data): void {
    this.dialog.open(DialogComponent, {
      width: '30%',
      data: data,
    });

  }
}
