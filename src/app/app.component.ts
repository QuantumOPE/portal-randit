import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'portal-randit';
  

  isLoggedIn(){
    return sessionStorage.getItem('user') ? false : true;
  }

  logout(){
    sessionStorage.removeItem('user');
    window.location.href='/login';
  }
}
