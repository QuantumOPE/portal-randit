import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { ProductService } from 'src/services/product.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalDialogComponent } from '../modal-dialog/modal-dialog.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css']
})
export class ProdutosComponent implements OnInit{

  constructor(private productApi: ProductService, public dialog: MatDialog) {
  }

  openDialog(productSelected): void {
    this.productSelected = productSelected;
    var dialogref = this.dialog.open(ModalDialogComponent, {
      width: '80%',
      data: this.productSelected
    });

    dialogref.afterClosed().subscribe(() => {
      this.listProducts();
    })
  }


  productList;

  productSelected = {
    "id": 0,
    "flagAtivo": false,
    "idMarca": 0,
    "nome": '',
    "caracteristicas": '',
    "descricao": '',
    "urlFotoPrincipal": '',
    "dtCadastro": '',
  }

  newProduct = {
    "id": 0,
    "flagAtivo": false,
    "idMarca": 0,
    "nome": '',
    "caracteristicas": '',
    "descricao": '',
    "urlFotoPrincipal": '',
    "dtCadastro": '',
  }

  async listProducts(){
    this.productList = await this.productApi.listProduct();
    this.productList = JSON.parse(this.productList).results
  }

  ngOnInit(){
    this.listProducts();  
  }

}
