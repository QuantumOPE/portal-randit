import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/services/user.service';
import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder } from '@angular/forms';
import { Md5 } from 'ts-md5';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  constructor(private userApi: UserService, public dialog: MatDialog, private formBuilder: FormBuilder) {
  }

  md5 = new Md5();

  submit: boolean = false;

  

  userList;

  userSelected = {
    "id": 0,
    "flagAtivo": false,
    "nome": '',
    "sobreNome": '',
    "numeroCpf": '',
    "email": '',
    "nomeUsuario": '',
    "senhaUsuario": '',
    "idUsuarioPerfilAcesso": 0,
    "idContatoTelefone": 0,
    "dtCadastro": '',
  }

  userModel = {
    "id": 0,
    "flagAtivo": false,
    "nome": '',
    "sobreNome": '',
    "numeroCpf": '',
    "email": '',
    "nomeUsuario": '',
    "senhaUsuario": '',
    "idUsuarioPerfilAcesso": 0,
    "idContatoTelefone": 0,
    "dtCadastro": '',
  }


  async listUser() {
    this.userList = await this.userApi.listUser();
    this.userList = JSON.parse(this.userList).results
    this.userSelected = this.userModel;
  }

  ngOnInit() {
    this.listUser();
  }


  selectUser(user) {
    this.userSelected = user;
  }

  newUser() {
    this.userSelected = this.userModel;
    let now = new Date();
    this.userSelected.dtCadastro = now.getDate() + '/' + ((+now.getMonth() + 1) < 10 ? '0' + (now.getMonth() + 1) : (now.getMonth() + 1)) + '/' + now.getFullYear()
  }

  async saveUser(user) {
    if (user.id != 0) {
      await this.userApi.updateUser(user).then((response) => {
        this.openDialog({ title: "Sucesso!", body: "Atualizações salvas.", ok: "Ok"})
      })
        .catch((error) => {
          this.openDialog({ title: "Erro!", body: "Não foi possível realizar a operação, tente novamente.", ok: "Ok"})
        });
    } else {
      await this.userApi.insertUser(user).then((response) => {
        this.openDialog({ title: "Sucesso!", body: "Usuário criado!", ok: "Ok"})
      })
        .catch((error) => {
          this.openDialog({ title: "Erro!", body: "Não foi possível realizar a operação, tente novamente.", ok: "Ok"})
        })
    }
  }

  async deleteUser(user) {
    this.userSelected = user;
    this.submit = true;
    this.openDialog({ title: "Deletar Usuário!", body: "Tem certeza que deseja deletar este usuário ?", ok: "Deletar", cancel: "Cancelar" })
  }

  openDialog(data): void {
    var dialogref = this.dialog.open(DialogComponent, {
      width: '30%',
      data: data,
    });

    dialogref.afterClosed().subscribe(async (data) => {
      if (data == 'ok' && this.submit == true) {
        await this.userApi.deleteUser(this.userSelected.id).then((response) => {
          this.openDialog({ title: "Sucesso!", body: "Usuário deletado.", ok: "Ok"})
          this.submit = false;
        })
          .catch((error) => {
            this.openDialog({ title: "Error!", body: "Não foi possível realizar a operação, tente novamente.", ok: "Ok"})
          })
      }
    });
  }

  reallySave(data): void {
    this.dialog.open(DialogComponent, {
      width: '30%',
      data: data,
    });

  }

}

// TODO: FAZER ABRIR O DIALOGO QUANDO SALVAR ALTERAÇÕES E COPIAR OS DIALOGOS PARA PRODUTOS E EMPRESAS;
