import { Component, OnInit } from '@angular/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { UserService } from 'src/services/user.service';
import { DialogComponent } from '../dialog/dialog.component';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {


  constructor(private userApi: UserService, public dialog: MatDialog, private formBuilder: FormBuilder, ) { }


  registerForm: FormGroup;
  p: FormControl;
  submit: boolean = false;

  user;

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      nomeUsuario: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      senhaUsuario: ['', Validators.required],
    });
  }

  async onSubmit(user) {
    if (this.registerForm.status == 'VALID') {
      await this.userApi.insertUser(user).then((response) => {
        this.submit = true;
        console.log(response)
        this.openDialog({ title: "Cadastro Realizado!", body: "Cadastro realizado com sucesso", ok: "Ok" });
      })
        .catch((error) => {
          this.openDialog({ title: "Formulário inválido!", body: "Preencha todos os campos", ok: "Ok" });
        })
    } else {
      this.openDialog({ title: "Formulário inválido!", body: "Preencha todos os campos", ok: "Ok" });
    }
  }

  showHide(event: any): any {
    this.user.company = event;
  }

  openDialog(data): void {
    var dialogref = this.dialog.open(DialogComponent, {
      width: '30%',
      data: data
    });

    dialogref.afterClosed().subscribe(() => {
      if (this.submit == true)
        window.location.href = "/login"
    });
  }


}
