import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from 'src/services/user.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private userApi: UserService, public dialog: MatDialog, private formBuilder: FormBuilder, ) { }


  registerForm: FormGroup;
  p: FormControl;
  submit: boolean = false;

  user = {
    email: '',
    senhaUsuario: ''
  }

  ngOnInit(): void {

    this.registerForm = this.formBuilder.group({
      email: [this.user.email, Validators.required],
      senhaUsuario: [this.user.senhaUsuario, Validators.required],
    });
  }

  async onSubmit(user: any) {
    this.submit = true
    if (this.registerForm.status == 'VALID') {
      this.user = user
      var k = await this.userApi.getUser(this.user).then((data) => {
        sessionStorage.setItem('user', JSON.stringify(data["results"][0]));
        window.location.href = "/products"
      })
        .catch((error) => {
          console.log(error)
          this.openDialog();
        });
    }
  }

  openDialog(): void {
    var dialogref = this.dialog.open(DialogComponent, {
      width: '30%',
      data: { title: "Ops!", body: "Usuário e/ou senha incorreta", ok: "Ok" }
    });
  }

}