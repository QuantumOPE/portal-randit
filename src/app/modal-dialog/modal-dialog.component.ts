import { Component, OnInit, Input, Output, Inject, OnDestroy } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogState, MatDialogClose, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ProductService } from 'src/services/product.service';

@Component({
  selector: 'app-modal-dialog',
  templateUrl: './modal-dialog.component.html',
  styleUrls: ['./modal-dialog.component.css']
})
export class ModalDialogComponent implements OnInit, OnDestroy {

  constructor(private productApi: ProductService,
              @Inject(MAT_DIALOG_DATA) public data,
              private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<ModalDialogComponent>) { }

  registerForm: FormGroup;
  p: FormControl;
  submit: boolean = false;

  ngOnInit(): void {

    this.registerForm = this.formBuilder.group({
      id: [this.data.id,],
      nome: [this.data.nome,],
      idMarca: [this.data.idMarca,],
      descricao: [this.data.descricao,],
      caracteristicas: [this.data.caracteristicas,],
      urlFotoPrincipal: [this.data.urlFotoPrincipal,],
      flagAtivo: [this.data.flagAtivo,],
      dtCadastro: [{value: this.data.dtCadastro, disabled: true},]
    });

  }

  async save(product){
    if(product.id != 0){
      await this.productApi.updateProduct(product).then(()=> {
        this.submit = true;
        this.dialogRef.close();
      })
      .catch((error)=>{
        console.log(error);
      });
    } else {
      await this.productApi.insertProduct(product).then(()=>{
        this.submit = true;
        this.dialogRef.close();
      })
      .catch((error)=>{
        console.log(error);
      })
    }
  }

  async delete(product){
    await this.productApi.deleteProduct(product.id).then(()=>{
      this.submit = true;
      this.dialogRef.close();
    })
    .catch((error) => {
      console.log(error);
    })
  }

  ngOnDestroy(){
    if(this.submit == true){
      this.dialogRef.close({data: this.registerForm.value});
    }
  }
}
