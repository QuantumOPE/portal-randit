import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable, throwError, Operator } from 'rxjs';
import { BinaryOperator } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  API_KEY = 'YOUR_API_KEY';

  

  public async listUser() {
    return this.httpClient.get('http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/identity/users').toPromise()
      .then(res => JSON.stringify(res))
      .catch(e => throwError(e.message));
  }

  public async getUser(data){
    return await this.httpClient.get('http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/identity/users', { params: data }).toPromise()
      .then(res => res)
      .catch(e => throwError(e.message));
  }

  public async insertUser(body) {
    return this.httpClient.post(`http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/identity/users`, body).toPromise()
      .then(res => JSON.stringify(res))
      .catch(e => {JSON.stringify(e);throwError(e.message)});
  }

  public async updateUser(body) {
    return this.httpClient.put(`http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/identity/users/${body.id}`, body).toPromise()
      .then(res => JSON.stringify(res))
      .catch(e => throwError(e.message));
  }

  public async deleteUser(id) {
    return this.httpClient.delete(`http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/identity/users/${id}`).toPromise()
      .then(res => JSON.stringify(res))
      .catch(e => throwError(e.message));
  }
}
