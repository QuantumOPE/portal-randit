import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  constructor(private httpClient: HttpClient) { }

  API_KEY = 'YOUR_API_KEY';


  public async listProduct() {
    return this.httpClient.get('http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/catalog/products').toPromise()
    .then(res => JSON.stringify(res))
    .catch(e => throwError(e.message));

  }

  public async getProduct(id) {
    return this.httpClient.get(`http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/catalog/products/${id}`).toPromise()
    .then(res => JSON.stringify(res))
    .catch(e => throwError(e.message));

  }

  public async insertProduct(body) {
    return this.httpClient.post(`http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/catalog/products`, body).toPromise()
    .then(res => JSON.stringify(res))
    .catch(e => throwError(e.message));

  }

  public async updateProduct(body) {
    return this.httpClient.put(`http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/catalog/products/${body.id}`, body).toPromise()
    .then(res => JSON.stringify(res))
    .catch(e => throwError(e.message));

  }

  public async deleteProduct(id) {
    return this.httpClient.delete(`http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/catalog/products/${id}`, { headers: new HttpHeaders({'Content-Type': 'application/json', 'access-control-allow-origin': '*'})}).toPromise()
    .then((res) => {console.log(res);JSON.stringify(res)})
    .catch(e => {console.log(e); Observable.throw(e.message)});

  }
}
