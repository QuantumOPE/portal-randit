import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class CompanyService {

  constructor(private httpClient: HttpClient) { }

  API_KEY = 'YOUR_API_KEY';


  public async listCompany() {
    return this.httpClient.get('http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/identity/companies').toPromise()
    .then(res => JSON.stringify(res))
    .catch(e => throwError(e.message));
  }

  public async getCompany(id) {
    return this.httpClient.get(`http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/identity/companies/${id}`).toPromise()
    .then(res => JSON.stringify(res))
    .catch(e => throwError(e.message));
  }

  public async insertCompany(body) {
    return this.httpClient.post(`http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/identity/companies`, body).toPromise()
    .then(res => JSON.stringify(res))
    .catch(e => throwError(e.message));
  }

  public async updateCompany(body) {
    return this.httpClient.put(`http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/identity/companies/${body.id}`, body).toPromise()
    .then(res => JSON.stringify(res))
    .catch(e => throwError(e.message));
  }

  public async deleteCompany(id) {
    return this.httpClient.delete(`http://ec2-18-230-71-213.sa-east-1.compute.amazonaws.com:5080/api/v1/identity/companies/${id}`).toPromise()
    .then(res => JSON.stringify(res))
    .catch(e => throwError(e.message));
  }
}
